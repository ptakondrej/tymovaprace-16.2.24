<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("./vendor/autoload.php");
$latte = new Latte\Engine();

if (!file_exists("./temp")) {
    mkdir("./temp");
}
$latte->setTempDirectory("./temp");

// Načtení dat z CSV souboru
$file = fopen('statistika.csv', 'r');
$headers = fgetcsv($file); // přečtení prvního řádku jako záhlaví
$data = [];
while (($row = fgetcsv($file)) !== false) {
    $rowData = [];
    foreach ($row as $key => $value) {
        $rowData[$headers[$key]] = $value; // přiřazení hodnot k odpovídajícím klíčům
    }
    $data[] = (object) $rowData; // přidání řádku do datového pole jako objekt
}
fclose($file);

$params = [
    "data" => $data,
];

// kresli na výstup
$latte->render("./template/statsTemplate.latte", $params);
