<?php

use Latte\Runtime as LR;

/** source: ./template/statsTemplate.latte */
final class Templateee82f6f8c3 extends Latte\Runtime\Template
{
	public const Source = './template/statsTemplate.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>AVAS Průměry</title>

    <style>

      body {
        display: flex;
        flex-direction: column;
        justify-content: center;
      }

      header h1 {
        text-align: center;
      }

      main {
        display: flex;
        flex-direction: column;
        justify-content: center;
      }

      table {
        border-collapse: collapse;
      }
      th,
      td {
        border: 1px solid #dddddd;
        padding: 8px;
      }
      td:first-child {
        background-color: #f2f2f2;
      }
      th {
        background-color: #f2f2f2;
      }
    </style>
  </head>
  <body>
    <header>
        <h1>Statistiky studentů oboru IT AVAS</h1>
    </header>
    <main>
        <table>
        <thead>
        <tr>
          <th>ID</th>
          <th>Průměr 1/1</th>
          <th>Průměr 1/2</th>
          <th>Průměr 2/1</th>
          <th>Průměr 2/2</th>
          <th>Průměr 3/1</th>
          <th>Průměr 3/2</th>
          <th>Průměr 4/1</th>
        </tr>
      </thead>
      <tbody>
';
		foreach ($data as $row) /* line 61 */ {
			echo '        <tr>
';
			foreach ($row as $cell) /* line 63 */ {
				echo '            <td>';
				echo LR\Filters::escapeHtmlText($cell) /* line 64 */;
				echo '</td>
';

			}

			echo '        </tr>
';

		}

		echo '      </tbody>
    </table>
    </main>
  </body>
</html>
';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['row' => '61', 'cell' => '63'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}
